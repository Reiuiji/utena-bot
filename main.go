package main

import (
	"errors"
	"fate-bot/bot"
	"fate-bot/gsheets"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
)

type config struct {
	bind           string
	sheetUrl       string
	cacheTimeout   int
	allowedOrigins string
	botType        string
	discordToken   string
}

func parseFlags() (config, error) {
	c := config{}
	flag.IntVar(&c.cacheTimeout, "cache-timeout", 300, "The timeout when a new copy of the schedule should be fetched. This applies also when the schedule cannot be fetched.")
	flag.StringVar(&c.bind, "bind", "0.0.0.0:8080", "The host:port to bind to.")
	flag.StringVar(&c.allowedOrigins, "allowed-origins", "", "The set of Origins that should be returned for requests.")
	flag.StringVar(&c.sheetUrl, "sheet-url", "", "The URL of the published Schedule Spreadsheet. Expected response is in CSV format.")
	flag.StringVar(&c.botType, "bot-type", "discord", "specify our bot interface")
	flag.StringVar(&c.discordToken, "discord-token", "", "Discord bot auth token")
	flag.Parse()

	if c.sheetUrl == "" {
		return c, fmt.Errorf("you must specify --sheet-url")
	}

	// Check what bot interface we are using
	if c.botType == "discord" {
		if c.discordToken == "" {
			return c, errors.New("--discord-token is required")
		}
	}

	return c, nil
}

func main() {
	c, err := parseFlags()
	if err != nil {
		log.Println(err)
		os.Exit(2)
	}

	f, err := gsheets.New(c.sheetUrl, c.cacheTimeout)
	if err != nil {
		log.Fatalf("couldn't read spread sheet: %v.\n", err)
	}

	if c.botType == "discord" {
		b, err := bot.New(c.discordToken, f)
		if err != nil {
			log.Fatalf("couldn't create bot: %v.\n", err)
		}
		go func() {
			err := b.RunForever()
			if err != nil {
				log.Printf("failed to run Dicord bot - %v\n", err)
			}
		}()
	}

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("ok"))
	})

	go func() {
		log.Printf("Serving on %s\n", c.bind)
		if err := http.ListenAndServe(c.bind, nil); err != nil {
			log.Fatalln(err)
		}
	}()

	// Setting up signal capturing
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Waiting for SIGINT (kill -2)
	<-stop
	log.Println("Shutting down bot")
}
