package fate

import (
	"fmt"
	"math/rand/v2"
)

const numRolls = 4

func randRange(min, max int) int {
	return rand.IntN(max-min+1) + min
}

func Roll() (int, string) {

	roll := map[int]string{
		-1: "-",
		0:  " ",
		+1: "+",
	}

	total := 0
	res := "["

	for i := 0; i < numRolls; i++ {
		randomRoll := randRange(-1, 1)
		res += roll[randomRoll]
		total += randomRoll
	}
	res += "]"

	return total, res
}

func PlayRoll(modifier int) (int, string) {

	roll := map[int]string{
		-1: "-",
		0:  " ",
		+1: "+",
	}

	val := 0
	dice := "["

	for i := 0; i < numRolls; i++ {
		randomRoll := randRange(-1, 1)
		dice += roll[randomRoll]
		val += randomRoll
	}
	dice += "]"

	total := modifier + val

	res := ""
	if modifier != 0 {
		res = fmt.Sprintf("%d + (%d) = %d %s", val, modifier, total, dice)
	} else {
		res = fmt.Sprintf("%d %s", total, dice)
	}

	return total, res
}
