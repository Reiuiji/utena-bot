package bot

import (
	"fate-bot/fate"
	"fate-bot/gsheets"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"log"
	"os"
	"os/signal"
	"strconv"
)

type FateBot struct {
	fateSheet    *gsheets.FateSheet
	discord      *discordgo.Session
	BidMessageID string
	discordGuild string
}

func New(discordToken string, fateSheet *gsheets.FateSheet) (*FateBot, error) {
	// Setup discord token
	d, err := discordgo.New("Bot " + discordToken)
	if err != nil {
		return nil, fmt.Errorf("couldn't create discord session: %v", err)
	}

	// Configure bot parameters
	b := &FateBot{
		fateSheet:    fateSheet,
		discord:      d,
		BidMessageID: "",
		discordGuild: "",
	}

	// Add handler for startup
	d.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		log.Printf("Discord: Logged in as: %v#%v", s.State.User.Username, s.State.User.Discriminator)
	})

	// Add handlers for interaction support (after we log in)
	d.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if h, ok := commandHandlers[i.ApplicationCommandData().Name]; ok {
			h(b, s, i)
		}
	})
	return b, nil
}

func (b *FateBot) RunForever() error {
	// connect to discord
	err := b.discord.Open()
	if err != nil {
		log.Printf("Cannot open the discord session: %v.\n", err)
		return err
	}
	// select {}
	defer func(discord *discordgo.Session) {
		err := discord.Close()
		if err != nil {
			log.Printf("Failed to close discord (We have major problems, time to explode): %v.\n", err)
		}
	}(b.discord)

	createdCommands, err := b.discord.ApplicationCommandBulkOverwrite(b.discord.State.User.ID, b.discordGuild, commands)

	if err != nil {
		log.Fatalf("Cannot register commands: %v", err)
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	<-stop

	for _, cmd := range createdCommands {
		err := b.discord.ApplicationCommandDelete(b.discord.State.User.ID, b.discordGuild, cmd.ID)
		if err != nil {
			log.Fatalf("Cannot delete %q command: %v", cmd.Name, err)
		}
	}
	log.Println("Discord: Shutting down interface")
	return nil
}

func (b *FateBot) handleRoll(s *discordgo.Session, i *discordgo.InteractionCreate) string {
	if i.Interaction.Member.User.ID == s.State.User.ID {
		return ""
	}
	userID := i.Interaction.Member.User.ID
	total, res := fate.Roll()
	response := fmt.Sprintf("<@%s> Rolled a %d %s", userID, total, res)
	return response
}

func (b *FateBot) handleCharacterRoll(s *discordgo.Session, i *discordgo.InteractionCreate) string {
	if i.Interaction.Member.User.ID == s.State.User.ID {
		return ""
	}
	userID := i.Interaction.Member.User.ID
	modifier := 0
	skill := ""
	specificPlayer := ""

	cmdData := i.ApplicationCommandData()
	for _, v := range cmdData.Options {
		switch v.Name {
		case "skill":
			skill = fmt.Sprintf("%v", v.Value)
		case "modifier":
			val, err := strconv.Atoi(fmt.Sprintf("%v", v.Value))
			if err == nil {
				modifier = val
			}
		case "player":
			specificPlayer = fmt.Sprintf("%v", v.Value)
		}
	}

	if modifier == 0 {
		player, ok := b.fateSheet.PlayersCache[userID]

		if specificPlayer != "" {
			ok = false
			for _, v := range b.fateSheet.PlayersCache {
				if v.Name == specificPlayer {
					player = v
					ok = true
				}
			}
		}
		if ok {
			if player.IsValid() {
				val, ok := player.Skills[skill]
				if ok {
					modifier = val
				}
			}
		}
	}

	val, dice := fate.Roll()
	total := val + modifier
	res := ""
	if modifier != 0 || skill != "" {
		res = fmt.Sprintf("%d = `%d + (%s:%d) %s`", total, val, skill, modifier, dice)
	} else {
		res = fmt.Sprintf("%d %s", total, dice)
	}
	response := ""
	if specificPlayer != "" {
		response = fmt.Sprintf("%s Rolled a %s", specificPlayer, res)
	} else {
		response = fmt.Sprintf("<@%s> Rolled a %s", userID, res)
	}
	return response
}

func (b *FateBot) printCharStats(s *discordgo.Session, i *discordgo.InteractionCreate) string {
	// check if the bot sends a message and ignore it
	if i.Interaction.Member.User.ID == s.State.User.ID {
		return ""
	}
	userID := i.Interaction.Member.User.ID
	player := b.fateSheet.PlayersCache[userID]
	if player.IsValid() {
		return player.Stats()
	}
	return "Character not found"
}

var (
	commands = []*discordgo.ApplicationCommand{
		{
			Name:        "roll",
			Description: "Roll a regular fate dice",
			Type:        discordgo.ChatApplicationCommand,
		},
		{
			Name:        "stat",
			Description: "Print out your character stats",
			Type:        discordgo.ChatApplicationCommand,
		},
		{
			Name:        "fate",
			Description: "(WIP) Roll fate dice based on your player info",
			Type:        discordgo.ChatApplicationCommand,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Name:         "skill",
					Description:  "Skill Name",
					Type:         discordgo.ApplicationCommandOptionString,
					Required:     false,
					Autocomplete: true,
				},
				{
					Name:        "modifier",
					Description: "Skill Modifier",
					Type:        discordgo.ApplicationCommandOptionString,
					Required:    false,
				},
				{
					Name:        "player",
					Description: "Select a specific player by name",
					Type:        discordgo.ApplicationCommandOptionString,
					Required:    false,
				},
			},
		},
	}

	commandHandlers = map[string]func(b *FateBot, s *discordgo.Session, i *discordgo.InteractionCreate){
		"roll": func(b *FateBot, s *discordgo.Session, i *discordgo.InteractionCreate) {
			content := b.handleRoll(s, i)
			log.Printf(i.Interaction.Member.User.Username + ": /roll - " + content)

			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				// Ignore type for now, they will be discussed in "responses"
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: content,
				},
			})
			if err != nil {
				log.Printf("Failed to create InteractionRespond for Discord: %v\n", err)
				return
			}
		},
		"stat": func(b *FateBot, s *discordgo.Session, i *discordgo.InteractionCreate) {
			content := b.printCharStats(s, i)
			log.Println(i.Interaction.Member.User.Username + ": /stat - " + content)
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: content,
				},
			})
			if err != nil {
				log.Printf("Failed to create InteractionRespond for Discord: %v\n", err)
				return
			}
		},
		"fate": func(b *FateBot, s *discordgo.Session, i *discordgo.InteractionCreate) {
			switch i.Type {
			case discordgo.InteractionApplicationCommand:
				content := b.handleCharacterRoll(s, i)
				log.Println(i.Interaction.Member.User.Username + ": /fate - " + content)
				err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: content,
					},
				})
				if err != nil {
					log.Printf("Failed to create InteractionApplicationCommand for Discord: %v\n", err)
					return
				}
			case discordgo.InteractionApplicationCommandAutocomplete:
				data := i.ApplicationCommandData()
				var choices []*discordgo.ApplicationCommandOptionChoice
				for _, v := range data.Options {
					if v.Focused == true {
						switch v.Name {
						case "skill":
							choices = []*discordgo.ApplicationCommandOptionChoice{
								{
									Name:  "Athletics",
									Value: "Athletics",
								},
								{
									Name:  "Burglary",
									Value: "Burglary",
								},
								{
									Name:  "Contacts",
									Value: "Contacts",
								},
								{
									Name:  "Crafts",
									Value: "Crafts",
								},
								{
									Name:  "Deceive",
									Value: "Deceive",
								},
								{
									Name:  "Drive",
									Value: "Drive",
								},
								{
									Name:  "Empathy",
									Value: "Empathy",
								},
								{
									Name:  "Fight",
									Value: "Fight",
								},
								{
									Name:  "Investigate",
									Value: "Investigate",
								},
								{
									Name:  "Lore",
									Value: "Lore",
								},
								{
									Name:  "Notice",
									Value: "Notice",
								},
								{
									Name:  "Physique",
									Value: "Physique",
								},
								{
									Name:  "Provoke",
									Value: "Provoke",
								},
								{
									Name:  "Rapport",
									Value: "Rapport",
								},
								{
									Name:  "Resources",
									Value: "Resources",
								},
								{
									Name:  "Shoot",
									Value: "Shoot",
								},
								{
									Name:  "Stealth",
									Value: "Stealth",
								},
								{
									Name:  "Will",
									Value: "Will",
								},
							}
						}
					}
				}

				err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionApplicationCommandAutocompleteResult,
					Data: &discordgo.InteractionResponseData{
						Choices: choices, // This is basically the whole purpose of autocomplete interaction - return custom options to the user.
					},
				})
				if err != nil {
					log.Printf("Failed to create InteractionRespond for Discord: %v\n", err)
					return
				}
			}
		},
	}
)
