package gsheets

import "fmt"

type Player struct {
	PlayerName string
	Discord    string
	Name       string
	Skills     map[string]int
}

func (player *Player) IsValid() bool {
	return player != nil &&
		player.PlayerName != "" &&
		player.Name != "" &&
		player.Discord != "" &&
		len(player.Skills) != 0
}

func (player *Player) Stats() string {
	if player.IsValid() {
		//res := "```\n"
		res := ""
		res += ">>> # " + player.Name + "\n"
		res += "### Skills:\n"
		for k, v := range player.Skills {
			res += fmt.Sprintf(" * %s:  %d\n", k, v)
		}
		//res += "```\n"
		//res += "\n"
		return res
	}

	return ""
}

type PlayerList map[string]Player

type sheetHeader struct {
	playerHeader  string
	discordHeader string
	nameHeader    string
	skillList     []string
}

type ErrHeaderParseFailed string

func (e ErrHeaderParseFailed) Error() string {
	return "Header parsing failed: " + string(e)
}

type ErrArgumentNil struct {
	ParamName string
	FuncName  string
}

func (e ErrArgumentNil) Error() string {
	return "Argument " + e.ParamName + " cannot be nil in " + e.FuncName
}
