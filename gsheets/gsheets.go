package gsheets

import (
	"bytes"
	"encoding/csv"
	"io"
	"log"
	"net/http"
	"reflect"
	"time"
)

type FateSheet struct {
	sheetUrl        string
	cacheTimeLength int
	webClient       http.Client
	eventCacheTime  time.Time
	PlayersCache    map[string]Player
}

func New(sheetUrl string, cacheTimeLength int) (*FateSheet, error) {

	f := &FateSheet{
		sheetUrl:        sheetUrl,
		cacheTimeLength: cacheTimeLength,
		webClient:       http.Client{},
		eventCacheTime:  time.Time{},
		PlayersCache:    map[string]Player{},
	}

	_, err := f.ReadEventCache()

	timeDuration := 30 * time.Second
	if cacheTimeLength != 0 {
		timeDuration = time.Duration(cacheTimeLength) * time.Second
	}

	ticker := time.NewTicker(timeDuration)
	go func() {
		for range ticker.C {
			_, err := f.ReadEventCache()
			if err != nil {
				log.Println("Failed to read data in ticker" + err.Error())
			}
		}
	}()

	return f, err
}

func (f *FateSheet) ReadEventCache() (map[string]Player, error) {
	if time.Now().After(f.eventCacheTime) {
		log.Printf("Reading Sheet data")
		err := f.updateEventCache(f.sheetUrl, f.cacheTimeLength)
		if err != nil {
			return nil, err
		}
	}
	return f.PlayersCache, nil
}

func (f *FateSheet) updateEventCache(sheetUrl string, cacheTimeLength int) error {

	rawEvents, err := f.fetchRawEvents(sheetUrl)
	if err != nil {
		log.Println("Unable to update event cache due to an error during fetchRawEvents: " + err.Error())
		return err
	}

	reader := csv.NewReader(bytes.NewReader(rawEvents))

	newPlayers, err := parseFate(reader)
	if err != nil {
		log.Println("Unable to update event cache due to an error during parseSchedule: " + err.Error())
		return err
	} else {
		if !reflect.DeepEqual(f.PlayersCache, newPlayers) {
			log.Printf("I got %d players\n", len(newPlayers))
			f.PlayersCache = newPlayers
		} else {
			log.Printf("No new data")
		}
		f.eventCacheTime = time.Now().Add(time.Duration(cacheTimeLength) * time.Second)
	}

	return nil
}

func (f *FateSheet) fetchRawEvents(sheetUrl string) ([]byte, error) {
	log.Printf("Going to call out to the following sheets URL: %s\n", sheetUrl)

	resp, err := f.webClient.Get(sheetUrl)
	if err != nil {
		log.Printf("Failed to fetch sheet with error: %s\n", err)
		return nil, err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Printf("Failed to read sheet with error: %s\n", err)
		}
	}(resp.Body)
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to read sheet with error: %s\n", err)
		return nil, err
	}

	return body, nil
}
