package gsheets

import (
	"encoding/csv"
	"io"
	"log"
	"strconv"
)

func parseFate(reader *csv.Reader) (map[string]Player, error) {

	// Find the position for player info and skills
	rowOne, err := reader.Read()
	if err != nil {
		log.Println("Unable to reader header line 1 due to error: " + err.Error())
		return nil, err
	}
	rowTwo, err := reader.Read()
	if err != nil {
		log.Println("Unable to reader header line 2 due to error: " + err.Error())
		return nil, err
	}

	header, skillPos, _, err := parseFateHeader(rowOne, rowTwo)
	if err != nil {
		return nil, err
	}

	// Go through and parse the players
	lineNumber := 2 // since we parsed the two header rows
	var row []string = nil
	players := make(PlayerList)
	for {
		row, err = reader.Read()
		if row == nil && err == io.EOF {
			break
		}
		// increment the line number since we've now read that line
		lineNumber++

		if err != nil {
			log.Println("Unable to complete parsing of schedule due to error: " + err.Error())
			return nil, err
		}
		err = parseScheduleRow(lineNumber, row, header, skillPos, players)
		if err != nil {
			log.Println("Unable to complete parsing of schedule due to error: " + err.Error())
			return nil, err
		}

	}

	return players, nil
}

func parseFateHeader(firstHeader []string, secondHeader []string) (sheetHeader, int, int, error) {
	var fateHeader = sheetHeader{}
	// Validation
	switch {
	case firstHeader == nil:
		return fateHeader, 0, 0, ErrHeaderParseFailed("firstRecord cannot be nil")
	case secondHeader == nil:
		return fateHeader, 0, 0, ErrHeaderParseFailed("secondRecord cannot be nil")
	case len(firstHeader) != len(secondHeader):
		return fateHeader, 0, 0, ErrHeaderParseFailed("Unable to parse schedule header due to mismatched first/second record length.")
	}

	// Calculate skill start
	var skillStartAt = 0
	for i := 1; i < len(firstHeader); i++ {
		if firstHeader[i] == "Skills" {
			skillStartAt = i
			break
		}
	}
	var skillCnt = len(firstHeader) - skillStartAt

	fateHeader.playerHeader = secondHeader[0]
	fateHeader.discordHeader = secondHeader[1]
	fateHeader.nameHeader = secondHeader[2]

	for i := skillStartAt; i < skillStartAt+skillCnt; i++ {
		fateHeader.skillList = append(fateHeader.skillList, secondHeader[i])
	}

	return fateHeader, skillStartAt, skillCnt, nil
}

func parseScheduleRow(_ int, row []string, header sheetHeader, skillPos int, players PlayerList) error {
	switch {
	case row == nil:
		return ErrArgumentNil{"row", "parseScheduleRow"}
	case players == nil:
		return ErrArgumentNil{"players", "parseScheduleRow"}
	}

	if row[0] == "" {
		return nil
	}
	var player = Player{}

	player.PlayerName = row[0]
	player.Discord = row[1]
	player.Name = row[2]

	skills := make(map[string]int)

	for i := 0; i < len(header.skillList); i++ {
		if row[skillPos+i] != "" {
			skillValue := 0
			val, err := strconv.Atoi(row[skillPos+i])
			if err == nil {
				skillValue = val
			}
			skills[header.skillList[i]] = skillValue
		}
	}

	player.Skills = skills

	if player.IsValid() {
		players[player.Discord] = player
	}

	return nil

}
